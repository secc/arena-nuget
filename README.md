Arena SECC NuGet
===============================================================================
**Note:** This project does not include the actual Arena DLL files since those 
are proprietary and licensed to each organization using Arena.  Please drop the 
ddl into the net40 (assuming you are using .NET 4) directory next to the 
corresponding .txt placeholder for the dll.

In order to proceed you'll need to host your packages locally or internally on an IIS
server on your local network.  We use NuGet.Server at SECC so all of our developers can
share the same packages without having to setup references for each project internally.
You can use the following guide to setup your NuGet server:
[https://docs.nuget.org/create/hosting-your-own-nuget-feeds](https://docs.nuget.org/create/hosting-your-own-nuget-feeds)

Versions
-----
Please note, the versions in the nuspec files in this repo are set for Arena version 2014.2.100.04114.  You can change the version information
in the nuspec files if you are running a different version of Arena.

Building
-----
To build a NuGet package, in a command prompt, change directory so you are in the same place as the nuspec file that 
corresponds to the package you want to build.  Then execute the command coresponding to the package you want to build
like the following commands:

> NuGet.exe pack Arena.dll.nuspec

> NuGet.exe pack Arena.Framework.dll.nuspec

> .....

[Southeast Christian Church](http://www.southeastchristian.org/)
------
Southeast Christian Church in Louisville, Kentucky is an evangelical Christian church. In our 
mission to connect people to Jesus and one another, Southeast Christian Church has 
grown into a unified multisite community located in the Greater Louisville/Southern Indiana region. 
We have multiple campuses serving the specific needs of the areas in which they are located, 
while receiving centralized leadership and teaching from the campus on Blankenbaker Parkway.